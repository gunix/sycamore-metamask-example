use ethers::core::types::Chain;
use ethers::etherscan::Client;
use serde::Serialize;
use sycamore::futures::spawn_local_scoped;
use sycamore::prelude::*;
use wasm_bindgen::prelude::*;

struct WalletIsConnected(bool);
struct WalletAddress(String);
struct WalletChain(String);
struct WalletBalance(String);
const ETH_REQUEST_ACCOUNTS: &str = "eth_requestAccounts";
const ETH_CHAIN_ID: &str = "eth_chainId";

pub async fn get_address() -> String {
    let response = ETHEREUM
        .request(
            &JsValue::from_serde(&MetaMaskRequest {
                method: ETH_REQUEST_ACCOUNTS.to_string(),
            })
            .unwrap(),
        )
        .await;
    match response.into_serde::<Vec<String>>() {
        Ok(v) => v[0].clone(),
        _ => "There was an error. Please report this bug.".to_string(),
    }
}

pub async fn get_chain_id() -> u64 {
    let response = ETHEREUM
        .request(
            &JsValue::from_serde(&MetaMaskRequest {
                method: ETH_CHAIN_ID.to_string(),
            })
            .unwrap(),
        )
        .await;
    let chain_string = response.into_serde::<String>().unwrap();
    u64::from_str_radix(&chain_string.trim_start_matches("0x"), 16).unwrap()
}

pub async fn get_balance(chain: Chain, pubkey: &str) -> String {
    Client::new(chain, "")
        .unwrap()
        .get_ether_balance_single(&pubkey.parse().unwrap(), None)
        .await
        .unwrap()
        .balance
}

#[component]
fn ConnectWallet<G: Html>(cx: Scope<'_>) -> View<G> {
    let is_connected = use_context::<RcSignal<WalletIsConnected>>(cx);
    let wallet_chain = use_context::<RcSignal<WalletChain>>(cx);
    let wallet_address = use_context::<RcSignal<WalletAddress>>(cx);
    let wallet_balance = use_context::<RcSignal<WalletBalance>>(cx);

    let get_account = move |_| {
        spawn_local_scoped(cx, async move {
            let address = get_address().await;
            wallet_address.set(WalletAddress(address.clone()));
            let chain_id = get_chain_id().await;
            let chain: Result<Chain, _> = chain_id.try_into();
            let (chain, balance) = match chain {
                Ok(chain) => (chain.to_string(), get_balance(chain, &address).await),
                Err(_) => (chain_id.to_string(), "Not supported.".to_string()),
            };
            wallet_chain.set(WalletChain(chain));
            wallet_balance.set(WalletBalance(balance));
            is_connected.set(WalletIsConnected(true));
        });
    };

    view!(cx, div {
        div(class="has-text-info has-text-centered is-size-5") {
            br {}
            p { "MetaMask wallet detected!" }
            br {}
            div(
                class="button is-medium is-outlines is-black has-text-info",
                on:click=get_account) {
                "Connect!"
            }
        }
    })
}

#[component]
fn WalletInfo<G: Html>(cx: Scope<'_>) -> View<G> {
    let chain = use_context::<RcSignal<WalletChain>>(cx);
    let address = use_context::<RcSignal<WalletAddress>>(cx);
    let balance = use_context::<RcSignal<WalletBalance>>(cx);
    view!(cx, div {
        p { "Connected!" }
        br {}
        table(class="container table is-info") {
            tr {
                th {"Chain:" }
                th { (chain.get().0) }
            }
            tr {
                th {"Address:" }
                th { (address.get().0) }
            }
            tr {
                th {"Balance:" }
                th { (balance.get().0) }
            }
        }
    })
}

#[component]
fn WalletIntegration<G: Html>(cx: Scope<'_>) -> View<G> {
    provide_context(
        cx,
        create_rc_signal(WalletAddress(String::from("Not connected."))),
    );
    provide_context(cx, create_rc_signal(WalletIsConnected(false)));
    provide_context(cx, create_rc_signal(WalletChain(String::new())));
    provide_context(cx, create_rc_signal(WalletBalance(String::new())));
    let is_connected = use_context::<RcSignal<WalletIsConnected>>(cx);

    view! { cx,
        div(class="has-text-centered is-size-5") {
            (if is_connected.get().0 {
                view!(cx, WalletInfo{})
            } else {
                view!(cx, ConnectWallet{})
            } )
        }
    }
}

#[component]
fn WalletNotFound<G: Html>(cx: Scope<'_>) -> View<G> {
    view!(cx, div {
        div(class="has-text-danger has-text-centered is-size-5") {
            br {}
            p { "MetaMask wallet not detected!" }
            br {}
            a(href="https://metamask.io/", target="_blank") {
                div(class="button is-medium is-outlines is-black has-text-danger") { "Get MetaMask!" }
            }
        }
    })
}

#[component]
fn App<G: Html>(cx: Scope<'_>) -> View<G> {
    let metamask_detected = create_signal(cx, ethereum_is_defined());
    view! { cx, div(class="container has-text-centered", style="font-family: 'Recursive', monospace;") {
        br {}
        div(class="title"){ "Sycamore+MetaMask Demo" }
        (if *metamask_detected.get() {
            view!(cx, WalletIntegration{})
        } else {
            view!(cx, WalletNotFound{})
        } )
        br {}
        a(class="has-text-link is-size-4",
            href="https://codeberg.org/gunix/sycamore-metamask-example/",
            target="_blank") {
            "Check the source code!"
        }
    }}
}

fn main() {
    console_error_panic_hook::set_once();
    console_log::init_with_level(log::Level::Debug).unwrap();
    sycamore::render(|cx| view! { cx, App() });
}

#[derive(Serialize)]
pub struct MetaMaskRequest {
    pub method: String,
}

#[wasm_bindgen]
extern "C" {
    pub type Ethereum;
    #[wasm_bindgen(js_name = ethereum)]
    pub static ETHEREUM: Ethereum;
    #[wasm_bindgen(method, catch, getter=isMetaMask)]
    pub fn is_metamask(this: &Ethereum) -> Result<bool, JsValue>;
    #[wasm_bindgen(method)]
    pub async fn request(this: &Ethereum, request: &JsValue) -> JsValue;
}

#[wasm_bindgen(module = "/js/ethereum.js")]
extern "C" {
    #[wasm_bindgen]
    fn ethereum_is_defined() -> bool;
}
